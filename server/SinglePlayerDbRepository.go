package server

import (
	"database/sql"
	"log"
	"path/filepath"
	"runtime"

	//registers SQL driver for sqlite
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/velimir2/bulls-and-cows/game"
)

//SinglePlayerDbRepository holds all data in memory (no DB access)
type SinglePlayerDbRepository struct {
	games []*game.Game
}

//CreateNewGame creates new game and adds it to the repository
func (repo *SinglePlayerDbRepository) CreateNewGame(playerName string) *game.Game {
	game := game.NewPendingGame(playerName)

	storeGameToDb(game)

	repo.games = append(repo.games, game)
	return game
}

//GetAllGames returns all games in the repository
func (repo *SinglePlayerDbRepository) GetAllGames() []*game.Game {
	return repo.games
}

//GetGame returns game by ID
func (repo *SinglePlayerDbRepository) GetGame(gameID int) *game.Game {
	for _, game := range repo.games {
		if game.ID == gameID {
			return game
		}
	}

	panic("No game with id " + string(gameID) + " found") //TODO return err
}

//Delete deletes game by ID from the DB
func (repo *SinglePlayerDbRepository) Delete(gameID int) {
	db := openDb()
	defer closeDb(db)

	runDeleteStmt(db, "DELETE FROM guesses WHERE GameId = ?", gameID)
	runDeleteStmt(db, "DELETE FROM games WHERE Id = ?", gameID)

	repo.games = delete(repo.games, gameID)
}

func runDeleteStmt(db *sql.DB, sqlStatement string, gameID int) {
	stmt, err := db.Prepare(sqlStatement)
	if err != nil {
		log.Fatal("Error preparing stmt: ", err)
	}
	defer closeStmt(stmt)

	_, err = stmt.Exec(gameID)

	if err != nil {
		log.Fatal("error executing delete stmt", err)
	}
}

//Load loads the repository with some data
func (repo *SinglePlayerDbRepository) Load() {
	db := openDb()
	defer closeDb(db)

	repo.loadGames(db)
	repo.loadGuesses(db)
}

func openDb() *sql.DB {
	dbPath := getDbFilePath()

	db, err := sql.Open("sqlite3", dbPath)
	if err != nil {
		log.Fatal("Error opening file: ", err)
	}

	return db
}

func closeDb(db *sql.DB) {
	err := db.Close()

	if err != nil {
		log.Println("Error closing db: ", err.Error())
	}
}

func (repo *SinglePlayerDbRepository) loadGames(db *sql.DB) {
	rows, err := db.Query("select ID, Name, SecretNumber, PlayerName from games")
	if err != nil {
		log.Fatal("DB query error: ", err)
	}

	defer closeRows(rows)

	for rows.Next() {
		var game game.Game

		err = rows.Scan(&game.ID, &game.Name, &game.Secret, &game.PlayerName)
		if err != nil {
			log.Fatal("Error scaning rows: ", err)
		}

		log.Println("Game restored: ", game)

		repo.games = append(repo.games, &game)
	}
	err = rows.Err()
	if err != nil {
		log.Fatal("Rows error: ", err)
	}
}

func (repo *SinglePlayerDbRepository) loadGuesses(db *sql.DB) {
	stmt, err := db.Prepare("select Attempt, PlayerName, Guess, Bulls, Cows from guesses where GameId = ?")
	if err != nil {
		log.Fatal(err)
	}
	defer closeStmt(stmt)

	for _, g := range repo.games {
		rows, err := stmt.Query(g.ID)
		if err != nil {
			log.Fatal(err)
		}
		defer closeRows(rows)

		for rows.Next() {
			var guess game.Guess
			err = rows.Scan(&guess.Number, &guess.PlayerName, &guess.PlayerGuess, &guess.Bulls, &guess.Cows)
			if err != nil {
				log.Fatal("Error scaning rows: ", err)
			}

			log.Println("Guess restored, game: ", g.ID, " guess: ", guess)

			g.AddGuess(guess)
		}

		err = rows.Err()
		if err != nil {
			log.Fatal("Rows error: ", err)
		}
	}
}

func storeGameToDb(game *game.Game) {
	db := openDb()
	defer closeDb(db)

	stmt, err := db.Prepare("insert into games (Name, SecretNumber, PlayerName) values (?, ?, ?)")
	if err != nil {
		log.Fatal(err)
	}
	defer closeStmt(stmt)

	res, err := stmt.Exec(game.Name, game.Secret, game.PlayerName)
	if err != nil {
		log.Fatal(err)
	}
	gameID, err := res.LastInsertId()
	if err != nil {
		log.Fatal(err)
	}

	game.ID = int(gameID)
}

//StoreGuess stores a newly made guess into the DB
func (repo *SinglePlayerDbRepository) StoreGuess(gameID int, guess game.Guess) {
	db := openDb()
	defer closeDb(db)

	stmt, err := db.Prepare("insert into guesses (GameId, Attempt, PlayerName, Guess, Bulls, Cows) values (?, ?, ?, ?, ?, ?)")
	if err != nil {
		log.Fatal(err)
	}
	defer closeStmt(stmt)

	_, err = stmt.Exec(gameID, guess.Number, guess.PlayerName, guess.PlayerGuess, guess.Bulls, guess.Cows)
	if err != nil {
		log.Fatal(err)
	}
}

func closeStmt(stmt *sql.Stmt) {
	err := stmt.Close()

	if err != nil {
		log.Println("Error closing statement: ", err.Error())
	}
}

func closeRows(rows *sql.Rows) {
	err := rows.Close()

	if err != nil {
		log.Println("Error closing rows: ", err.Error())
	}
}

func getDbFilePath() string {
	_, filename, _, _ := runtime.Caller(0)

	callerPath := filepath.Dir(filename)
	log.Println("Caller path: ", callerPath)

	dbPath := callerPath + "/../db/games.db"
	log.Println("db path: ", dbPath)

	return dbPath
}
