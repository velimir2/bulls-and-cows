package main

import (
	"fmt"
	"math/rand"
	"net/http"
	"time"

	"gitlab.com/velimir2/bulls-and-cows/server"
)

func main() {
	var singlePlayerRepository server.SinglePlayerInMemoryRepository
	//var singlePlayerRepository server.SinglePlayerDbRepository
	singlePlayerRepository.Load()

	var s server.Server
	s.Init(&singlePlayerRepository)

	rand.Seed(time.Now().UTC().UnixNano())

	fs := http.FileServer(http.Dir("web"))
	http.Handle("/", fs)

	http.HandleFunc("/api/single", s.ServeSinglePlayerGames)
	http.HandleFunc("/api/multi", s.ServeMultiPlayerGames)

	http.HandleFunc("/api/new-single", s.NewSinglePlayerGame)
	http.HandleFunc("/api/single/guess", s.MakeSinglePlayerGuess)
	http.HandleFunc("/api/delete-single", s.DeleteSinglePlayerGame)

	fmt.Println("Listening to http://localhost:8080/ ...")
	err := http.ListenAndServe(":8080", nil)

	if err != nil {
		fmt.Println("Error running http server", err.Error())
	}
}
