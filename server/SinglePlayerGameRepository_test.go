package server

import "testing"

func TestCreateNewGameAddsGameToTheRepository(t *testing.T) {
	var repository SinglePlayerGameRepository = &SinglePlayerInMemoryRepository{}
	repository.Load()

	repository.CreateNewGame("Boris")

	games := repository.GetAllGames()

	if len(games) != 1 {
		t.Error("Repository was not loaded with data. Elements after loading: ", len(games))
	}
}

func TestGetGameReturnsCorrectResult(t *testing.T) {
	var repository SinglePlayerGameRepository = &SinglePlayerInMemoryRepository{}
	repository.Load()
	repository.CreateNewGame("Boris")

	game := repository.GetGame(1)

	if game == nil || game.PlayerName != "Boris" {
		t.Error("Unable to find game by ID")
	}
}

func TestDeleteRemovesGameFromTheRepository(t *testing.T) {
	var repository SinglePlayerGameRepository = &SinglePlayerInMemoryRepository{}
	repository.Load()
	repository.CreateNewGame("Boris")

	repository.Delete(1)

	if len(repository.GetAllGames()) != 0 {
		t.Error("Unable to delete game by ID")
	}
}
