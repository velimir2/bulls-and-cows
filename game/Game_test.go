package game

import "testing"

func TestMakingGuessReturnsCorrectResult(t *testing.T) {
	game := NewGameWithKnownSecret(1, "5678", "Bob")

	guessResult := game.MakeGuess("1234")

	if guessResult.Number != 1 {
		t.Error("Guess counter not updated on new guess")
	}
	if guessResult.Bulls != 0 {
		t.Error("Incorrect number of bulls when no number matches")
	}
	if guessResult.Cows != 0 {
		t.Error("Incorrect number of cows when no number matches")
	}
	if game.State != InProgress {
		t.Error("Game was not in progress when making the first guess")
	}
}

func TestMakingGuessWithOneCowReturnsCorrectResult(t *testing.T) {
	game := NewGameWithKnownSecret(1, "5678", "Bob")

	guessResult := game.MakeGuess("1235")

	if guessResult.Bulls != 0 {
		t.Error("Incorrect number of bulls when no number matches")
	}
	if guessResult.Cows != 1 {
		t.Error("Incorrect number of cows when one number on wrong position matches")
	}
}

func TestMakingGuessWithOneBull(t *testing.T) {
	game := NewGameWithKnownSecret(1, "5678", "Bob")

	guessResult := game.MakeGuess("5123")

	if guessResult.Bulls != 1 {
		t.Error("Incorrect number of bulls when one expected")
	}
	if guessResult.Cows != 0 {
		t.Error("Incorrect number of cows when no match matches")
	}
}

func TestMakingGuessWithTwoRepeatingCows(t *testing.T) {
	game := NewGameWithKnownSecret(1, "5678", "Bob")

	guessResult := game.MakeGuess("5523")

	if guessResult.Bulls != 1 {
		t.Error("Incorrect number of bulls when one expected")
	}
	if guessResult.Cows != 1 {
		t.Error("Incorrect number of cows when one matches (but is also a bull?!)")
	}
}

func TestMakingGuessWithTwoRepeatingNumbersInSecret(t *testing.T) {
	game := NewGameWithKnownSecret(1, "5678", "Bob")

	guessResult := game.MakeGuess("1255")

	if guessResult.Bulls != 0 {
		t.Error("Incorrect number of bulls when none matches")
	}
	if guessResult.Cows != 2 {
		t.Error("Incorrect number of cows when two repeating in the guess are matching")
	}
}

func TestMakingCorrectGuess(t *testing.T) {
	game := NewGameWithKnownSecret(1, "5678", "Bob")

	guessResult := game.MakeGuess("5678")

	if guessResult.Bulls != 4 {
		t.Error("Incorrect number of bulls when all are matching")
	}
	if guessResult.Cows != 0 {
		t.Error("Incorrect number of cows when none are matching")
	}
	if game.State != Finished {
		t.Error("Game was not finished when making a correct guess")
	}
}

func TestMakingGuessWith4Cows(t *testing.T) {
	game := NewGameWithKnownSecret(1, "5678", "Bob")

	guessResult := game.MakeGuess("8765")

	if guessResult.Bulls != 0 {
		t.Error("Incorrect number of bulls when none are matching")
	}
	if guessResult.Cows != 4 {
		t.Error("Incorrect number of cows when all are matching")
	}
}
