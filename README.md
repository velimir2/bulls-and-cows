# Bulls & Cows game in Go

- Web interface with static html and js files
- Backend in Go

## Running extended check:

```Shell
go fmt ./... && go vet ./... && golint ./... && errcheck ./... && go test ./... -cover && go install ./...
```

## DB access
The server works by default with InMemory repository. To switch to DB repository change the first lines of the main method.
The DB in use is sqlite
- Get sqlite from http://sqlite.org/
- Get sqlite browser from http://sqlitebrowser.org/
- Install sqlite driver from https://github.com/mattn/go-sqlite3

## How to play
1. Build the server
2. Run the executable
3. Open a browser and navigate to http://localhost:8080
4. On the open page enter player name and click Enter
5. Click new single player game
6. Enter a guess and press Send. Repeat until you get 4 cows :)
7. For an auto solution just click on the "Auto solve (Bot)" link
