package game

import (
	"encoding/json"
	"fmt"
)

//possible game states
const (
	NewGame    = 0
	InProgress = 1
	Finished   = 2
)

//Game a bulls and cows game
type Game struct {
	ID           int
	Name         string
	Secret       string
	PlayerName   string
	Guesses      []Guess
	State        int
	guessCounter int
}

//MakeGuess make a new guess in a game
func (g *Game) MakeGuess(guess string) Guess {
	if len(guess) != 4 {
		panic("unexpected guess format")
	}

	g.State = InProgress

	bulls, cows := 0, 0

	for index := range guess {
		gi := guess[index] - 48
		si := g.Secret[index] - 48

		if gi == si {
			bulls++
		} else {
			for i := range g.Secret {
				s := g.Secret[i] - 48
				if s == gi {
					cows++
				}
			}
		}
	}
	fmt.Println("Guess: ", guess, " Bulls: ", bulls, ", cows: ", cows)

	g.guessCounter++
	newGuess := Guess{g.guessCounter, g.PlayerName, guess, bulls, cows}
	g.Guesses = append(g.Guesses, newGuess)

	if bulls == 4 {
		g.State = Finished
	}
	return newGuess
}

//AddGuess adds already existing guess to the game (like when loading from the DB)
func (g *Game) AddGuess(guess Guess) {
	g.Guesses = append(g.Guesses, guess)

	if g.guessCounter < guess.Number {
		g.guessCounter = guess.Number
	}
}

//NewSinglePlayerGame creates new single player game with default values
func NewSinglePlayerGame(id int, playerName string) *Game {
	return newGame(id, "Single player game", playerName)
}

//NewMultiPlayerGame creates new single player game with default values
func NewMultiPlayerGame(id int) *Game {
	return newGame(id, "Multiplayer game", "No player yet")
}

//TODO: multiplayer game has two players
func newGame(id int, nameTemplate string, playerName string) *Game {
	var g = Game{
		id,
		fmt.Sprintf("%s %d", nameTemplate, id),
		newSecret(),
		playerName,
		make([]Guess, 0),
		0,
		0,
	}
	return &g
}

//NewGameWithKnownSecret create game with know id, secret and player name
func NewGameWithKnownSecret(id int, secret string, playerName string) *Game {
	var g = Game{
		id,
		"Single player game " + string(id),
		secret,
		playerName,
		make([]Guess, 0),
		0,
		0,
	}
	return &g
}

//NewPendingGame create game without id, to be updated later when added to DB
func NewPendingGame(playerName string) *Game {
	var g = Game{
		0,
		"Single player game for " + playerName,
		newSecret(),
		playerName,
		make([]Guess, 0),
		0,
		0,
	}
	return &g
}

//MarshalJSON marshals game to JSON (only selected fields)
func (g *Game) MarshalJSON() ([]byte, error) {
	return json.Marshal(&struct {
		ID      int
		Name    string
		State   int
		Guesses []Guess
	}{
		g.ID,
		g.Name,
		g.State,
		g.Guesses,
	})
}
