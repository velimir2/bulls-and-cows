package game

import (
	"fmt"
	"math/rand"
)

//Secret number that the players must guess
type Secret [4]int

func newSecret() string {
	s1 := rand.Intn(9) + 1 // make sure first num is not 0

	s2 := s1
	for s2 == s1 {
		s2 = rand.Intn(10)
	}

	s3 := s1
	for s3 == s1 || s3 == s2 {
		s3 = rand.Intn(10)
	}

	s4 := s1
	for s4 == s1 || s4 == s2 || s4 == s3 {
		s4 = rand.Intn(10)
	}

	return fmt.Sprintf("%d%d%d%d", s1, s2, s3, s4)
}
