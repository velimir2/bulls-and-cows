package server

import (
	"gitlab.com/velimir2/bulls-and-cows/game"
)

//SinglePlayerGameRepository retrieve and store games
type SinglePlayerGameRepository interface {
	Load()
	CreateNewGame(playerName string) *game.Game
	StoreGuess(gameID int, guess game.Guess)
	GetAllGames() []*game.Game
	GetGame(gameID int) *game.Game
	Delete(gameID int)
}
