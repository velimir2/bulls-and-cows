FROM golang:latest AS builder
WORKDIR /go/src/gitlab.com/velimir2/bulls-and-cows
COPY . .
RUN cd $WORKDIR;
RUN go get github.com/mattn/go-sqlite3; go install github.com/mattn/go-sqlite3 
RUN go build

ENTRYPOINT ["./bulls-and-cows"]
LABEL Name=bulls-and-cows Version=1.0.0
EXPOSE 8080
