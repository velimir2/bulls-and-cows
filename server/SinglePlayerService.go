package server

import (
	"gitlab.com/velimir2/bulls-and-cows/game"
)

//SinglePlayerService manages all single player games
type SinglePlayerService struct {
	repository SinglePlayerGameRepository
}

//NewGame creates new game and returns it
func (service *SinglePlayerService) NewGame(playerName string) *game.Game {
	game := service.repository.CreateNewGame(playerName)
	return game
}

//GetAllGames returns all single player games
func (service *SinglePlayerService) GetAllGames() []*game.Game {
	return service.repository.GetAllGames()
}

//MakeGuess new guess in an existing game
func (service *SinglePlayerService) MakeGuess(gameID int, guess string) *game.Game {
	game := service.repository.GetGame(gameID)

	newGuess := game.MakeGuess(guess)
	service.repository.StoreGuess(gameID, newGuess)
	return game
}

//Delete deletes a game
func (service *SinglePlayerService) Delete(gameID int) {
	service.repository.Delete(gameID)
}
