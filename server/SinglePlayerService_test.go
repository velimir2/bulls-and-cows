package server

import (
	"fmt"
	"testing"
)

func TestNewGame_CreatesNewGameAndReturnsIt(t *testing.T) {
	var repository SinglePlayerInMemoryRepository
	singlePlayerService := SinglePlayerService{&repository}
	game := singlePlayerService.NewGame("Boris")

	if game.ID != 1 {
		t.Error("Unexpected Id of new game ", game.ID)
	}

	if game.Name != "Single player game 1" {
		t.Error("Unexpected Name of new game ", game.Name)
	}

	games := singlePlayerService.GetAllGames()
	if len(games) != 1 {
		t.Error("Unexpected number of games after creating the first one: ", len(games))
	}

	if game.Secret[0] == 0 {
		t.Error("Secret number cannot start with 0")
	}

	for num := range game.Secret {
		fmt.Println("Secret num[i]: ", num)
		if num < 0 || num > 9 {
			t.Error("Unexpected number in secret: ", num)
		}
	}
}

func TestMakeGuessAddsNewGuessToExistingGame(t *testing.T) {
	var repository SinglePlayerInMemoryRepository
	service := SinglePlayerService{&repository}
	game := service.NewGame("Boris")

	service.MakeGuess(game.ID, "1234")

	if len(game.Guesses) != 1 {
		t.Error("Newly made guess was not added to an existing game")
	}
}
