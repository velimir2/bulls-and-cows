var playerName;
var activeSinglePlayerGameID;
var activeSinglePlayerGame;
var singlePlayerGames = [];

$(document).ready(function() {    
    $.getJSON("/api/single")
        .done(function(games) {
            reloadGames(games);
        })
        .fail(function(jqxhr, textStatus, error) {
            $("#singlePlayerGames").html("Error getting single player games from the server " + error + ", " + textStatus);
    });

    $.getJSON("/api/multi")
        .done(function(data) {
            $("#multiPlayerGames").empty();
            $.each(data, function(i, item) {
                $("#multiPlayerGames").append("<li>" + item.Name + "</li>");
            });
        })
        .fail(function(jqxhr, textStatus, error) {
            $("#multiPlayerGames").html("Error getting multiplayer games from the server " + error + ", " + textStatus);
    });
});

function reloadGames(games) {
    singlePlayerGames = games;
    $("#singlePlayerGames").empty();
    $.each(games, function(i, game) {
        appendSinglePlayerGame(game);
    });
}

function enterPlayerName() {
    playerName = $("#playerName").val();
    $("#playerGreeting").text("Hello " + playerName);
}

function startNewSinglePlayer() {
    $.post("/api/new-single", JSON.stringify({ name: playerName }))
        .done(function(game) {
            singlePlayerGames.push(game);
            appendSinglePlayerGame(game);
            setActiveSinglePlayerGame(game);
      });
}

function appendSinglePlayerGame(game) {
    $("#singlePlayerGames").append("<li>" + game.Name + 
        ", <a href='#' onclick='setActiveSinglePlayerGameById(" + game.ID + ")'>Solve (Single player)</a>" +
        ", <a href='#' onclick='autoSolveById(" + game.ID + ")'>Auto solve (bot)</a>" +
        ", <a href='#' onclick='deleteGame(" + game.ID + ")'>Delete</a>" +
        "</li>");
}

function setActiveSinglePlayerGameById(gameID) {
    $.each(singlePlayerGames, function(i, game) {
        if (game.ID === gameID) {
            setActiveSinglePlayerGame(game);
        }
    });
}

function autoSolveById(gameID) {
    $.each(singlePlayerGames, function(i, game) {
        if (game.ID === gameID) {
            setActiveSinglePlayerGame(game);
            autoSolve(game);
        }
    });
}

var allRemainingPossibilities = [];

function autoSolve(game) {
    allRemainingPossibilities = createAllPossibilities();
    makeNextAutoGuess();
}

function makeNextAutoGuess() {
    var nextGuess;
    if (allRemainingPossibilities.length == 1) {
        nextGuess = allRemainingPossibilities[0];
    } else {
        var randomIndex = Math.floor(Math.random() * allRemainingPossibilities.length);
        nextGuess = allRemainingPossibilities[randomIndex];
    }

    makeSinglePlayerAutoGuess(nextGuess);
}

function makeSinglePlayerAutoGuess(guess) {
    var postData = { gameID: activeSinglePlayerGame.ID, guess: guess};

    $.post("/api/single/guess", JSON.stringify(postData))
        .done(function(game) {
            setActiveSinglePlayerGame(game);

            var lastResult = game.Guesses[game.Guesses.length - 1];
            if (lastResult.Bulls < 4) {
                learnFrom(lastResult);
                makeNextAutoGuess();
            } else {
                alert("The bot solved it in " + game.Guesses.length + " attempts. Can you beat him?");
            }
      });
}

function learnFrom(lastResult) {
    var reducedPossibilities = [];

    for (i = 0; i < allRemainingPossibilities.length; i++) {
        var possibility = allRemainingPossibilities[i];
        var bullsCows = getBullsAndCows(possibility, lastResult.PlayerGuess);

        if (bullsCows.Bulls == lastResult.Bulls && bullsCows.Cows == lastResult.Cows) {
            reducedPossibilities.push(possibility);
        }
    }

    allRemainingPossibilities = reducedPossibilities;
}

function getBullsAndCows(possible, guess) {
    var bulls = 0;
    var cows = 0;

    for (pi = 0; pi < 4; pi++)
    {
        var c = guess[pi];
        if (possible.indexOf(c) == -1)
            continue;
        if (possible[pi] == c)
            bulls++;
        else
            cows++;
    }

    return { Bulls: bulls, Cows: cows};
}

function deleteGame(gameID) {
    $.post("/api/delete-single", JSON.stringify({ GameId: gameID }))
        .done(function(games) {
            reloadGames(games);
    });
}

function setActiveSinglePlayerGame(game) {
    activeSinglePlayerGame = game;
    $("#activeGameName").text(game.Name);

    $("#activeSpGame").empty();
    $("#activeSpGame").append("<tr><th>Turn</th><th>Guess</th><th>Result</th></tr>");
    $.each(game.Guesses, function(i, guess) {
        $("#activeSpGame").append("<tr><td>" + guess.Number + "</td><td>" + guess.PlayerGuess + "</td><td>" + "Bulls: " + guess.Bulls + ", Cows: " + guess.Cows +"</td></tr>");
    });
}

function makeNewSinglePlayerGuess() {
    var guess = $("#spGuess").val();
    var postData = { gameID: activeSinglePlayerGame.ID, guess: guess};

    $.post("/api/single/guess", JSON.stringify(postData))
        .done(function(game) {
            setActiveSinglePlayerGame(game);
      });

      $("#spGuess").val("");
}

function createAllPossibilities() {
    var possibleChars = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    var result = [];

    var possibleChars0 = createPossibleChars(possibleChars, 0);
    for (i0 = 0; i0 < possibleChars0.length; i0++) {
        var possibleChars1 = createPossibleChars(possibleChars0, possibleChars0[i0]);

        for (i1 = 0; i1 < possibleChars1.length; i1++) {
            var possibleChars2 = createPossibleChars(possibleChars1, possibleChars1[i1]);

            for (i2 = 0; i2 < possibleChars2.length; i2++) {
                var possibleChars3 = createPossibleChars(possibleChars2, possibleChars2[i2]);

                for (i3 = 0; i3 < possibleChars3.length; i3++) {
                    var combination = possibleChars0[i0].toString() + possibleChars1[i1].toString() + possibleChars2[i2].toString() + possibleChars3[i3].toString();
                    result.push(combination);
                }
            }
        }
    }

    return result;
}

function createPossibleChars(possibleChars, notLikeThisElement) {
    var result = [];
    for (i = 0; i < possibleChars.length; i++) {
        if (possibleChars[i] != notLikeThisElement){
            result.push(possibleChars[i]);
        }
    }
    return result;
}