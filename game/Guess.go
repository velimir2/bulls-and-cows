package game

//Guess a guess made by a player
type Guess struct {
	Number      int
	PlayerName  string
	PlayerGuess string
	Bulls       int
	Cows        int
}
