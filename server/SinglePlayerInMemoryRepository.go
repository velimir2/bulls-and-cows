package server

import "gitlab.com/velimir2/bulls-and-cows/game"

//SinglePlayerInMemoryRepository holds all data in memory (no DB access)
type SinglePlayerInMemoryRepository struct {
	games     []*game.Game
	idCounter int
}

//CreateNewGame creates new game and adds it to the repository
func (repo *SinglePlayerInMemoryRepository) CreateNewGame(playerName string) *game.Game {
	repo.idCounter++
	game := game.NewSinglePlayerGame(repo.idCounter, playerName)
	repo.games = append(repo.games, game)
	return game
}

//GetAllGames returns all games in the repository
func (repo *SinglePlayerInMemoryRepository) GetAllGames() []*game.Game {
	return repo.games
}

//GetGame returns game by ID
func (repo *SinglePlayerInMemoryRepository) GetGame(gameID int) *game.Game {
	for _, game := range repo.games {
		if game.ID == gameID {
			return game
		}
	}

	panic("No game with id " + string(gameID) + " found") //TODO return err
}

//Load loads the repository with some data
func (repo *SinglePlayerInMemoryRepository) Load() {
	repo.games = make([]*game.Game, 0)
}

//StoreGuess stores newly made guess to the repo
func (repo *SinglePlayerInMemoryRepository) StoreGuess(gameID int, guess game.Guess) {
	//empty, game puts it into it's field
}

//Delete deletes game from the repo
func (repo *SinglePlayerInMemoryRepository) Delete(gameID int) {
	repo.games = delete(repo.games, gameID)
}

func delete(games []*game.Game, gameID int) []*game.Game {
	indexToDelete := -1
	for index, game := range games {
		if game.ID == gameID {
			indexToDelete = index
		}
	}

	if indexToDelete != -1 {
		games[indexToDelete] = games[len(games)-1] // Copy last element to index i
		games[len(games)-1] = nil                  // Erase last element (write zero value)
		games = games[:len(games)-1]               // Truncate slice
	}

	return games
}
