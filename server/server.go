package server

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/velimir2/bulls-and-cows/game"
)

//Server the game server
type Server struct {
	singlePlayerService *SinglePlayerService
}

//Init inits the server with all necessary dependencies
func (server *Server) Init(singlePlayerRepository SinglePlayerGameRepository) {
	server.singlePlayerService = &SinglePlayerService{singlePlayerRepository}
}

//NewSinglePlayerGame creates new game and writes data about it to the response
func (server *Server) NewSinglePlayerGame(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var requestData struct{ Name string }
	err = json.Unmarshal(body, &requestData)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	game := server.singlePlayerService.NewGame(requestData.Name)
	fmt.Println("game created, ", game)

	jsonGameData, err := json.Marshal(game)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	_, err = w.Write(jsonGameData)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

//MakeSinglePlayerGuess player makes a guess in single player
func (server *Server) MakeSinglePlayerGuess(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var requestData struct {
		GameID int
		Guess  string
	}
	err = json.Unmarshal(body, &requestData)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	fmt.Println(requestData)

	game := server.singlePlayerService.MakeGuess(requestData.GameID, requestData.Guess)

	jsonGameData, err := json.Marshal(game)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	_, err = w.Write(jsonGameData)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

//DeleteSinglePlayerGame deletes a game
func (server *Server) DeleteSinglePlayerGame(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var requestData struct {
		GameID int
	}
	err = json.Unmarshal(body, &requestData)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	fmt.Println(requestData)

	server.singlePlayerService.Delete(requestData.GameID)
	games := server.singlePlayerService.GetAllGames()

	jsonGameData, err := json.Marshal(games)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	_, err = w.Write(jsonGameData)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

//ServeSinglePlayerGames writes to response writer all single player games as JSON
func (server *Server) ServeSinglePlayerGames(w http.ResponseWriter, r *http.Request) {
	games := server.singlePlayerService.GetAllGames()
	jData, err := json.Marshal(games)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	_, err = w.Write(jData)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

//ServeMultiPlayerGames writes to response writer all multiplayer games as JSON
func (server Server) ServeMultiPlayerGames(w http.ResponseWriter, r *http.Request) {
	games := []*game.Game{
		game.NewMultiPlayerGame(1), game.NewMultiPlayerGame(2),
	}
	jData, err := json.Marshal(games)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	fmt.Println("games: ", games)
	fmt.Println("serving multiplayer games: ", string(jData))

	w.Header().Set("Content-Type", "application/json")
	_, err = w.Write(jData)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
