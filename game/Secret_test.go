package game

import "testing"

func TestGeneratingCorrectSecrets(t *testing.T) {
	for i := 0; i < 10000; i++ {
		s := newSecret()

		t.Log("Secret: ", s)

		if s[0]-'0' == 0 {
			t.Error("secret number cannot start with 0")
		}

		for index, si := range s {
			for index2, si2 := range s {
				if index == index2 {
					continue
				}

				if si == si2 {
					t.Error("secret number cannot cannot contain two equal numbers")
					return
				}
			}
		}
	}
}
